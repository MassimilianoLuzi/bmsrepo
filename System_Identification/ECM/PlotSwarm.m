function [fig] = PlotSwarm( figureHandle, swarm, gBest, Fitness, fitnessOptions, iter)
fig=figure(figureHandle);
clf;

% Swarm
subplot(2,1,1)
hold on;

position = [swarm.position];
positionLength = length(gBest(1).position);

plot(position, ':o');

color = hsv(length(gBest));
for b = 1:length(gBest)
    plot(gBest(b).position, 'p-', 'MarkerSize', 10, 'color', color(b,:));
end


xlim([1, positionLength]);
hold off;

xlabel('position Index');
ylabel('Normalized Value');
title(sprintf('Swarm at iter %d\n\rMSE; %d', iter, gBest(1).fitness));

%% Best Vout
% Get the samples
index = 1;

% Vist
% Instantaneous resistor
Rist = position(index);
index = index+1;

% Vdyn
tauDyn = zeros(fitnessOptions.numTau,1);
Rdyn = zeros(fitnessOptions.numTau,1);
for s=1:fitnessOptions.numTau
    tauDyn(s)  = fitnessOptions.maxTau*position(index);
    Rdyn(s) =  position(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

% Vqst - OCV
% ocvSamples = position{index};
% index = index+1;
ocvSamples = zeros(fitnessOptions.numOcvSamples,1);
for s=1:fitnessOptions.numOcvSamples
    ocvSamples(s) = position(index);
    index=index+1;
end
% Setup the interpolation points
delta = 1/(fitnessOptions.numOcvSamples-1);
interpolationPoints = 0 : delta : 1;
xInterpolateVector = 0:1/(fitnessOptions.numInterpPoints-1):1;
% Evaluate OCV (Vqst) by interpolating the achieved samples
VqstSamples  = interp1(interpolationPoints, ocvSamples, xInterpolateVector, 'PCHIP')';
% Generate the related vector of SoC of the OCV-SoC function
SoCVector = linspace(0,1,fitnessOptions.numInterpPoints)';


%% Store extracted Parameters
batteryParameters = struct(              ...
    'Cn',           fitnessOptions.Cn,             ...
    'eta',          fitnessOptions.eta,              ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'SoCVector',    SoCVector,      ...
    'VqstSamples',  VqstSamples);

%% Evaluate MSE
% Evaluate Vdyn with the parameters encoded in the position 
Vout_estimated = GetOutputVoltage(batteryParameters, fitnessOptions.numTau, fitnessOptions.Iin, fitnessOptions.Ts);

subplot(2,1,2)
plot(fitnessOptions.Vout);
hold on
plot(Vout_estimated);
drawnow;