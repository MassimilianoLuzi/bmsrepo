function [Vout, state] = GetOutputVoltage( batteryParameters, numTau, Iin, Ts)
% GetOutputVoltage: the function evaluate the output solving the Finite Difference
% Equation relataed to the linear version of the battery model

% Initialize state
state = zeros(length(Iin)+1,1+numTau);
state(1,1) = 1;     % Initial SoC=1 (full charged cell)
state(1,2:end)=0;   % Initial dynamic contribution set to zero (stationary contition)

% Instantiate variable for Vout
Vout = zeros(length(Iin),1);

% Evaluate Vout
for n=1:length(Iin)
    state(n+1,:) = EvaluateState(state(n,:), Iin(n), batteryParameters, Ts);  % x[k+1] = f(x[k], u[k])
    Vout(n) = EvaluateOutput(state(n,:), Iin(n), batteryParameters);          % y[k] = g(x[k], u[k])
end

