function out = lookupTable( input, breakpoints, values, extrapolation )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin<4
    extrapolation = 'clip';
end

N = length(breakpoints);
if N~=length(values)
    error('Breakpoints and values vector are not of the same length');
end

if input >= breakpoints(end)
    if strcmp(extrapolation, 'clip')
        out = values(end);
    elseif strcmp(extrapolation, 'linear')
        dx = breakpoints(end)-breakpoints(end-1);
        dy = values(end)-values(end-1);
        out = values(end) + (input-breakpoints(end))*(dy/dx);
    end
    return;
    
elseif input <= breakpoints(1)
    if strcmp(extrapolation, 'clip')
        out = values(1);
    elseif strcmp(extrapolation, 'linear')
        dx = breakpoints(2)-breakpoints(1);
        dy = values(2)-values(1);
        out = values(1) + (input-breakpoints(1))*(dy/dx);
    end
    return ;
end

startIndex = 1;
endIndex = N;
for k=1:ceil(log2(N))
    centralIndex = round((startIndex+endIndex)/2);
    if (input-breakpoints(centralIndex)>=0 && input-breakpoints(centralIndex+1)<=0)
        break;
    elseif input > breakpoints(centralIndex)
        startIndex = centralIndex+1;
    else
        endIndex = centralIndex-1;
    end   
end

index = centralIndex;
dx = breakpoints(index+1)-breakpoints(index);
dy = values(index+1)-values(index);

out = values(index) + (input-breakpoints(index))*(dy/dx);