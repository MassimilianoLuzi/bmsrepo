function outState = EvaluateState( state, input, batteryParam, Ts )
% EvaluateState: This is the process function f() of the battery model
% Inputs:
% state: the current state (x[k])
% input: the current input (u[k])
% batteryParam: struct storing all the parameters of the model
%
% output: the next state (x[k+1])

% Get the leghth of states
N = length(state);

% Initialize the variable outState
outState = zeros(N,1);

% The first state is the SoC eavaluated with the Coloumb Counting approach
% (discrete approximation of the integral)
outState(1) = state(1) + (Ts*input)/(batteryParam.Cn*3600);

% The other states refer to each RC group
for n=1:N-1
    % x[k+1] = exp(-1/tau)*x[k] + u[k]*R*(1-exp(-1/tau))
    outState(n+1) = exp(-Ts/batteryParam.tauDyn(n))*state(n+1) + input*batteryParam.Rdyn(n)*(1-exp(-Ts/batteryParam.tauDyn(n)));
end
end

