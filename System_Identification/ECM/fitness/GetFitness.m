function  fitness = GetFitness( particle, fitnessParam)
% GetFitness: Evaluate the fitness based on Mean Square Error
% input: 
% particle: the current particle of the PSO
% fitnessParam: struct containing the parameters needed for the fitness
% evaluation


%% Get the battery parameters from the current particle
% Particle structure
% |Rist, Taudyn(1), Rdyn(1), Taudyn(2), Rdyn(2), ... , Taudyn(numTau), Rdyn(numTau) OCV(1),OCV(2), ... , OCV(numOCVSamples), |   

% Get the samples
index = 1;

%% Vist
% Instantaneous resistor
Rist = particle(index);
index = index+1;

%% Vdyn
tauDyn = zeros(fitnessParam.numTau,1);
Rdyn = zeros(fitnessParam.numTau,1);
for s=1:fitnessParam.numTau
    tauDyn(s)  = fitnessParam.minTau + (fitnessParam.maxTau-fitnessParam.minTau)*particle(index);
    Rdyn(s) =  particle(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
% ocvSamples = particle{index};
% index = index+1;
ocvSamples = zeros(fitnessParam.numOcvSamples,1);
for s=1:fitnessParam.numOcvSamples
    ocvSamples(s) = particle(index);
    index=index+1;
end
% Setup the interpolation points
delta = 1/(fitnessParam.numOcvSamples-1);
interpolationPoints = 0 : delta : 1;
xInterpolateVector = 0:1/(fitnessParam.numInterpPoints-1):1;
% Evaluate OCV (Vqst) by interpolating the achieved samples
VqstSamples  = interp1(interpolationPoints, ocvSamples, xInterpolateVector, 'PCHIP')';
% Generate the related vector of SoC of the OCV-SoC function
SoCVector = linspace(fitnessParam.minSoC, fitnessParam.maxSoC, fitnessParam.numInterpPoints)';


%% Store extracted Parameters
batteryParameters = struct(             ...
    'Cn',           fitnessParam.Cn,    ...
    'Rist',         Rist,               ...
    'Rdyn',         Rdyn,               ...
    'tauDyn',       tauDyn,             ...
    'Cdyn',         tauDyn./Rdyn,       ...
    'SoCVector',    SoCVector,          ...
    'VqstSamples',  VqstSamples);

%% Evaluate MSE
% Evaluate Vdyn with the parameters encoded in the particle 
Vout_estimated = GetOutputVoltage(batteryParameters, fitnessParam.numTau, fitnessParam.Iin, fitnessParam.Ts);
mse = immse(fitnessParam.Vout, Vout_estimated);

fitness = mse;


end