% Script for the automatic parameter extraction of a cell from the
% acuisition structured as follows:
%
% Online identification of parameters with PSO algorithm

clear;
close all;
clc;

% Import the HGPSO algorithm. Ensure +heuristic_optimizer is in the MATLAB
% path
import heuristic_optimizer.HGPSO.*

% add path
addpath(genpath('fitness'));


%% Load Training Set
[acquisitionName, acquisitionPath] = uigetfile('Acquisition','Select Training Set', '../../../Dataset');
load(strcat(acquisitionPath,acquisitionName));

tr_Vout = Vout;
tr_Iin = Iin;
tr_Time = Time;

%% Normalize
headroom = 0.1;
maxInputVTr = max(tr_Vout);
minInputVTr = min(tr_Vout);

delta = headroom*(maxInputVTr-minInputVTr);
maxInputVTr = maxInputVTr + delta/2;
minInputVTr = minInputVTr - delta/2;

maxInputITr = (1+headroom)*max(abs(tr_Iin));

tr_Iin = Iin;
tr_Vout = Vout;
tr_SoC = SoC;
tr_Time = Time;

tr_Vout_normalized = (tr_Vout - minInputVTr)/(maxInputVTr-minInputVTr);
tr_Iin_normalized = tr_Iin/maxInputITr;

minSoC = min(tr_SoC);
maxSoC = max(tr_SoC);

%% Load Test Set
[acquisitionName, acquisitionPath] = uigetfile('Acquisition','Select Test Set', '../../../DataSet');
load(strcat(acquisitionPath,acquisitionName));

ts_Iin = Iin;
ts_Vout = Vout;
ts_SoC = SoC;
ts_Time = Time;

ts_Vout_normalized = (ts_Vout - minInputVTr)/(maxInputVTr-minInputVTr);
ts_Iin_normalized = ts_Iin/maxInputITr;


%% Ask for modle configuration
prompt = {...
    'Model Name',           ...
    'Number of RC groups',  ...
    'Maximum Tau Value',    ...
    'Minimmum Tau Value',   ...
    'numOcvSamples',        ...
    'numInterpPoints'       ...
    };

diagOption.Resize = 'on';
diagOption.WindowsStyle = 'normal';
defaultAnser = {'#ModelName', '3', '10000', '10', '15', '1000'};
promptTitle = 'Enter information about the selected acquisition';
inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption); 

% Model name for the output file
modelName = inputParameters{1};
numTau = str2double(inputParameters{2});               
maxTau = str2double(inputParameters{3});
minTau = str2double(inputParameters{4});
numOcvSamples = str2double(inputParameters{5});
numInterpPoints = str2double(inputParameters{6});   

% Cell capacity Cn should be included in dataset, if not ask to the user
if ~exist('Cn', 'var')
    prompt = {'Cell Capacity'};
    
    diagOption.Resize = 'on';
    diagOption.WindowsStyle = 'normal';
    defaultAnser = {'Cn'};
    promptTitle = 'Enter cell capacity';
    inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption);
    
    % Model name for the output file
    Cn = str2double(inputParameters{1});
end

% Sample time Ts should be included in dataset, if not ask to the user
if ~exist('Ts', 'var')
    prompt = {'Sample Time'};
    
    diagOption.Resize = 'on';
    diagOption.WindowsStyle = 'normal';
    defaultAnser = {'Ts'};
    promptTitle = 'Enter cell capacity';
    inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption);
    
    % Model name for the output file
    Ts = str2double(inputParameters{1});
end


%% Configure PSO
% Particle structure
% | Rist, Taudyn(1), Rdyn(1), Taudyn(2), Rdyn(2), ... , Taudyn(numTau), Rdyn(numTau), OCV(1),OCV(2), ... , OCV(numOCVSamples)|   
particleLength = numOcvSamples + 2*numTau + 1;

%Set algorithm parameters 
lb = zeros(particleLength, 1);
Ub = inf(particleLength, 1); 
generalOptions = struct(...
        'parallelFlag',             true,       ... flag for the parallel evaluation
        'adaptiveFlag',             false,      ... flag for the adaptive velocity update
        'numIndividuals',           50,         ... num individuals of the pso
        'initializationMode',       'uniform',	... type of initialization (zero, uniform, normal)
        'initializationRange',      [0, 1],     ... range of the initialization
        'lb',                       lb,         ... lower bounds
        'Ub',                       Ub,         ... upper bounds
        'numBests',                 2,          ... Number of subswarms
        'killRatio',                0.05,       ... Ratio for kill the subswarm in case of few individuals
        'guaranteedDelta',          0.1,        ... max variation allowed for guaranteed convergence pso
        'maxVelocity',              0.1,        ... max variation allowed for velocity
        'maxIteration',             1000,       ... max num of iteration
        'consecutiveTrueTh',        inf,         ... threshold for the stop condition
        'stopThreshold',            1e-8,       ... threhsold on the difference between old and new gBest
        'fitnessStopThreshold',     1e-8        ... threshold on the difference between old best fitness and new best fitness
        );

% Set PSO parameters
PSOPOptions = struct(  ...
    'w',     0.7298,      ...   Inertial coefficient
    'c_p',   1.49618,     ...   Weight of personal best
    'c_g',   1.49618      ...   Weight of global best
    );

GAOptions = struct(  ...
    'kRate',         0.25,          ... kRate for K tournament used in GA hybridization
    'mutationRate',  0.1,           ... Percentage of numIndividuals defining the number of mutation    
    'mutationTh',    0.75,          ... Threshold for the gene to mutate
    'mutationStd',   0.1,             ... Std for mutate the position
    'crossoverRate', 0.1,           ... Percentage of numIndividuals defining the number of crossovers  
    'crossoverTh',   0.5            ... Threshold for the gene to mutate  
    );

% Set Fitness parameters
fitnessOptions = struct(                          ...
    'Iin',                  tr_Iin_normalized,  ... Normalized input current
    'Vout',                 tr_Vout_normalized, ... normalized output voltage
    'numOcvSamples',        numOcvSamples,      ... num of samples for approximating the Open Circuit Voltage
    'numInterpPoints',      numInterpPoints,    ... num of interpolation points for the cubic interpolation of the numOcvSamples samples.
    'numTau',               numTau,             ... num of RC group for modeling the dynamic response
    'maxTau',               maxTau,             ... maximum value for the time constant. To be used only for normalization.
    'minTau',               minTau,             ... minimum value for the time constant. To be used only for normalization.
    'Cn',                   Cn/maxInputITr,     ... Normalized capacity of the cell
    'minSoC',               minSoC,             ... minSoC
    'maxSoC',               maxSoC,             ... maxSoC
    'Ts',                   Ts                  ... Sampling time
    );
    
%% Run PSO
disp('Running PSO...');
hgpsoOptimizer= HGPSO(@GetFitness, particleLength, ...
    'generalOptions',   generalOptions, ...
    'fitnessOptions',   fitnessOptions, ...
    'PSOOptions',       PSOPOptions,    ...
    'GAOptions',        GAOptions);

hgpsoOptimizer = hgpsoOptimizer.Initialize();
[hgpsoOptimizer, bestParticle, bestFitness, history, convergenceTime, convergenceIter] = hgpsoOptimizer.Run();

% Get the samples
index = 1;

%% Vist
% Instantaneous resistor
Rist = bestParticle(index);
index = index+1;

%% Vdyn
tauDyn = zeros(fitnessOptions.numTau,1);
Rdyn = zeros(fitnessOptions.numTau,1);
for s=1:fitnessOptions.numTau
    tauDyn(s) = fitnessOptions.maxTau*bestParticle(index);
    Rdyn(s) =  bestParticle(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
% ocvSamples = particle{index};
% index = index+1;
ocvSamples = zeros(fitnessOptions.numOcvSamples,1);
for s=1:fitnessOptions.numOcvSamples
    ocvSamples(s) = bestParticle(index);
    index=index+1;
end
% Setup the interpolation points
delta = 1/(fitnessOptions.numOcvSamples-1);
interpolationPoints = 0 : delta : 1;
xInterpolateVector = 0:1/(fitnessOptions.numInterpPoints-1):1;
% Evaluate OCV (Vqst) by interpolating the achieved samples
VqstSamples  = interp1(interpolationPoints, ocvSamples, xInterpolateVector, 'PCHIP')';
% Generate the related vector of SoC of the OCV-SoC function
SoCVector = linspace(minSoC,maxSoC,fitnessOptions.numInterpPoints)';


%% Store extracted Parameters
disp('Storing extracted parameters...');
batteryParameters_normalized = struct(              ...
    'Cn',           fitnessOptions.Cn,             ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'SoCVector',    SoCVector,      ...
    'VqstSamples',  VqstSamples);

disp('Denormalizing extracted parameters...');
batteryParameters = batteryParameters_normalized;
batteryParameters.VqstSamples = minInputVTr + batteryParameters.VqstSamples*(maxInputVTr-minInputVTr); 
batteryParameters.Rist = batteryParameters.Rist*(maxInputVTr-minInputVTr)/maxInputITr;
batteryParameters.Rdyn = batteryParameters.Rdyn*(maxInputVTr-minInputVTr)/maxInputITr;
batteryParameters.Cdyn = batteryParameters.Cdyn*maxInputITr/(maxInputVTr-minInputVTr);
batteryParameters.Cn = batteryParameters.Cn*maxInputITr;

%% Test extracted parameters
disp('Testing the model...');
[tr_Vout_estimated_normalized, tr_state_normalized] = GetOutputVoltage(batteryParameters_normalized, numTau, tr_Iin_normalized, Ts);
tr_mse_normalized = immse(tr_Vout_normalized, tr_Vout_estimated_normalized);

tr_Vout_estimated = minInputVTr + tr_Vout_estimated_normalized*(maxInputVTr - minInputVTr);

figure
plot(tr_Time/3600, tr_Vout);
hold on;
plot(tr_Time/3600, tr_Vout_estimated, 'r');
title(sprintf('Training Set\nMSE: %d', tr_mse_normalized));
xlabel('Time [h]');
ylabel('Voltage [V]');
legend('Measured Voltage', 'Estimated Voltage');

[ts_Vout_estimated_normalized, ts_state_normalized] = GetOutputVoltage(batteryParameters_normalized, numTau, ts_Iin_normalized, Ts);
ts_mse = immse(ts_Vout_normalized, ts_Vout_estimated_normalized);

ts_Vout_estimated = minInputVTr + ts_Vout_estimated_normalized*(maxInputVTr - minInputVTr);

figure
plot(ts_Time/3600, ts_Vout);
hold on;
plot(ts_Time/3600, ts_Vout_estimated, 'r');
title(sprintf('Test Set\nMSE: %d', ts_mse));
xlabel('Time [h]');
ylabel('Voltage [V]');
legend('Measured Voltage', 'Estimated Voltage');

mkdir('Output')
save(strcat('Output/batteryParameters_',modelName));



