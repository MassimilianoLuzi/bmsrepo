import os
import time
import mock
import json
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from tkinter.filedialog import askopenfilename, asksaveasfilename
from keras import initializers
from keras.models import load_model, model_from_json
from keras import backend as K
from customRecurrent import LeakyRNN
from customLayers import FunctionalLink

def tau_range_init(shape, name=None, dtype=K.floatx()):
    tau = 10 * np.logspace(1, 3, shape[0], dtype=dtype)
    x = 1 - (K.exp(-1 / tau))
    value = K.log(x / (1 - x))
    return value


# Workaround for saving and loading model
old_init_get = initializers.get
def patch_get(x):
    return tau_range_init if x == 'tau_range_init' else old_init_get(x)

@mock.patch('keras.initializers.get', patch_get)
def load_json(jsonModelFile):
    with open(jsonModelFile) as data_file:
        jsonData = json.load(data_file)
    input_scaling = jsonData['input_scaling']
    loaded_model_json = json.dumps(jsonData)
    network = model_from_json(loaded_model_json, custom_objects={'LeakyRNN': LeakyRNN, 'FunctionalLink': FunctionalLink})
    network.load_weights(os.path.splitext(jsonModelFile)[0]+'_Weights.h5')
    return input_scaling, network

### LOAD MODEL ###
modelJsonFile = askopenfilename(initialdir=os.path.abspath('Models'), title='Load Trained Model',
                                defaultextension='h5', filetypes=(("json file", "*.json"), ("All Files", "*.*")))
input_scaling, full_network = load_json(modelJsonFile)
full_network.compile(optimizer='Nadam', loss='mse')

### DEFINE FNC FOR GETTING SINGLE NETWORKS OUTPUTS

# Define the output functions
outIstFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input],
                       [full_network.get_layer(name='OutIstNet').output])

outDynFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input],
                       [full_network.get_layer(name='OutDynNet').output])

outQstFnc = K.function([full_network.get_layer(name='SoC').input],
                       [full_network.get_layer(name='OutQstNet').output])


### READ TEST SET ###
testFile = askopenfilename(initialdir=os.path.abspath('../DataSet/'), title='Load Test Data', defaultextension='mat',
                           filetypes=(("mat file", "*.mat"), ("All Files", "*.*")))

dataTs = scipy.io.loadmat(testFile)
ts_Time = dataTs['Time']
ts_Iin = dataTs['Iin']
ts_SoC = dataTs['SoC']
ts_Vout = dataTs['Vout']
ts_Temp = dataTs['Temp'] if 'Temp' in dataTs else np.zeros(ts_Iin.shape)
ts_Vqst = dataTs['Vqst'] if 'Vqst' in dataTs else np.zeros(ts_Iin.shape)
ts_Vdyn = dataTs['Vdyn'] if 'Vdyn' in dataTs else np.zeros(ts_Iin.shape)
ts_Vist = dataTs['Vist'] if 'Vist' in dataTs else np.zeros(ts_Iin.shape)

maxInputVTr = input_scaling['maxInputVTr']
minInputVTr = input_scaling['minInputVTr']
maxInputITr = input_scaling['maxInputITr']

# Normalize test set data
ts_Vout = (ts_Vout - minInputVTr) / (maxInputVTr - minInputVTr)
ts_Vqst = (ts_Vqst - minInputVTr) / (maxInputVTr - minInputVTr)
ts_Vdyn /= (maxInputVTr - minInputVTr)
ts_Vist /= (maxInputVTr - minInputVTr)
ts_Iin /= maxInputITr
ts_Temp /= 100

# Define Inputs
SoC = ts_SoC #- 0.5
ts_SoC = np.clip(SoC, a_min=0, a_max=1)

# Reshape Test Set as a tuple fo temporal sequences
inputTs_Iin = np.reshape(ts_Iin, (ts_Iin.shape[1], ts_Iin.shape[0], 1))
inputTs_SoC = np.reshape(ts_SoC, (ts_SoC.shape[1], ts_SoC.shape[0], 1))
inputTs_Temp = np.reshape(ts_Temp, (ts_Temp.shape[1], ts_Temp.shape[0], 1))
outputTs = np.reshape(ts_Vout, (ts_Vout.shape[1], ts_Vout.shape[0], 1))


# Test Set
startWatch = time.time()
outputTest = full_network.predict_on_batch([inputTs_Iin, inputTs_SoC])
testTime = time.time() - startWatch
print('Elapsed Time: ', testTime)

# Evaluate loss on test set
totalLossTest = full_network.test_on_batch([inputTs_Iin, inputTs_SoC], outputTs)

# Evaluate single components
outIstTest = outIstFnc([inputTs_Iin, inputTs_SoC])[0]  # Ist Net
outDynTest = outDynFnc([inputTs_Iin, inputTs_SoC])[0]  # Dyn Net
outQstTest = outQstFnc([inputTs_SoC])[0]  # Qst Net


log = {
    'Model': os.path.splitext(os.path.basename(modelJsonFile))[0],
    'TestData': os.path.splitext(os.path.basename(testFile))[0],
    'LossFunction': full_network.loss,
    'Optimizer': full_network.optimizer.__str__(),
    'Description': 'Test of the pretrained Model'
     }

# Save results in matlab file
if not os.path.exists('Output Test'):
    os.mkdir('Output Test')
defaultFileName = os.path.splitext(os.path.basename(testFile))[0] + '_' + time.ctime().replace(':', '-')
outMatlabFile = asksaveasfilename(title='Save Results for Matlab',
                                  initialdir=os.path.abspath('Output Test'), initialfile=defaultFileName,
                                  defaultextension='mat', filetypes=(("mat file", "*.mat"), ("All Files", "*.*")))

scipy.io.savemat(outMatlabFile, {
    #Log
    'log': log,
    # Normalization Data
    'maxInputVTr': maxInputVTr,
    'minInputVTr': minInputVTr,
    'maxInputITr': maxInputITr,
    # Real Inputs and Outputs Test
    'ts_Time': ts_Time,
    'ts_Iin': ts_Iin,
    'ts_SoC': ts_SoC,
    'ts_Vout': ts_Vout,
    'ts_Vist': ts_Vist,
    'ts_Vdyn': ts_Vdyn,
    'ts_Vqst': ts_Vqst,
    # Network Inputs
    'inputTs_Iin': inputTs_Iin,
    'inputTs_SoC': inputTs_SoC,
    'inputTs_Temp': inputTs_Temp,
    # Network Outputs
    'outTest': outputTest,
    'outQstTest': outQstTest,
    'outIstTest': outIstTest,
    'outDynTest': outDynTest,
    # Network Weights
    'testMse': totalLossTest,
    'testTime': testTime
    })

# Plotting test set results
plt.figure()
plt.plot(outputTs[0, :])
plt.plot(outputTest[0, :])
plt.title('Total Estimation - Test Set\n mse: %.4e' % totalLossTest)
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()


plt.figure()
plt.plot(ts_Vist)
plt.plot(outIstTest[0, :])
plt.title('Instantaneous Contribution Estimation - Test Set')
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()


plt.figure()
plt.plot(ts_Vdyn)
plt.plot(outDynTest[0, :])
plt.title('Dynamic Contribution Estimation - Test Set')
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()


plt.figure()
plt.plot(ts_Vqst)
plt.plot(outQstTest[0, :])
plt.title('Quasi-stationary Contribution Estimation - Test Set')
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()