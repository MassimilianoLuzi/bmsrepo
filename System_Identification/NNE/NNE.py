import time

import numpy as np

import keras
from keras.models import Model
from keras.layers import Input, Dense, TimeDistributed
from keras.layers.merge import Add, Concatenate
from keras import backend as K

import sys
sys.path.append('../../Libraries/Python')

from customLayers import FunctionalLink
from customRecurrent import LeakyRNN


class NNE:
    def __init__(self, Ts, SoC_in, Temp_in,
                 num_neurons_Ist=10,
                 num_leaky=5,
                 num_cheby=0, num_bernstein=20, num_trig=10, num_bspline=0,
                 outputActivation_Qst='sigmoid',
                 ):

        # Save parameters
        # General parameters
        self.Ts = Ts
        self.SoC_in = SoC_in
        self.Temp_in = Temp_in

        # Vist net
        self.num_neurons_Ist = num_neurons_Ist

        # Vdyn net
        self.num_leaky = num_leaky

        # Vqst net
        self.num_cheby = num_cheby
        self.num_bernstein = num_bernstein
        self.num_trig = num_trig
        self.num_bspline = num_bspline
        self.outputActivation_Qst = outputActivation_Qst


        ### BUILD NETWORK ###
        # Define input layers
        Iin = Input(shape=(None, 1), name='Iin')
        SoC = Input(shape=(None, 1), name='SoC')
        Temp = Input(shape=(None, 1), name='Temp')

        # Build parameteric input
        if Temp_in:
            if SoC_in:
                componentInput = Concatenate()([Iin, Temp, SoC])
            else:
                componentInput = Concatenate()([Iin, Temp])
        else:
            if SoC_in:
                componentInput = Concatenate()([Iin, SoC])
            else:
                componentInput = Iin

        # Initialize Input of each network
        Vist = componentInput
        Vdyn = componentInput

        if Temp_in:
            Vqst = Concatenate()([SoC, Temp])
        else:
            Vqst = SoC

        # Vist Net
        Vist = TimeDistributed(Dense(self.num_neurons_Ist, activation='tanh', kernel_initializer='glorot_normal', use_bias=False),
                               name='HidIstNet')(Vist)
        Vist = TimeDistributed(Dense(1, activation='linear', kernel_initializer='glorot_normal', use_bias=False),
                               name='OutIstNet')(Vist)

        # Build dynamic network
        Vdyn = LeakyRNN(self.num_leaky, activation='tanh', use_bias=False, kernel_initializer='zero',
                        recurrent_initializer=self.tau_range_init,
                        return_sequences=True, name='HidDynNet')(Vdyn)
        Vdyn = TimeDistributed(Dense(1, activation='linear', kernel_initializer='glorot_normal', use_bias=False),
                               name='OutDynNet')(Vdyn)

        # Build Vqst network
        Vqst = TimeDistributed(FunctionalLink(num_cheby=self.num_cheby, num_trig=self.num_trig, num_bernstein=self.num_bernstein,
                                              num_bspline=self.num_bspline),
                               name='HidQstNet')(Vqst)
        Vqst = TimeDistributed(Dense(1, activation=self.outputActivation_Qst, kernel_initializer='zero'), name='OutQstNet')(Vqst)

        # Build full network
        Vout = Add()([Vist, Vqst, Vdyn])

        full_network = Model(inputs=[Iin, SoC], outputs=Vout)

        #Define the overall network
        if Temp_in:
            self.net = Model(inputs=[Iin, SoC, Temp], outputs=Vout)

            # Define the output functions
            self.VistFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input,
                                    full_network.get_layer(name='Temp').input],
                                   [full_network.get_layer(name='OutIstNet').output])

            self.VdynFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input,
                                    full_network.get_layer(name='Temp').input],
                                   [full_network.get_layer(name='OutDynNet').output])

            self.VqstFnc = K.function(
                [full_network.get_layer(name='SoC').input, full_network.get_layer(name='Temp').input],
                [full_network.get_layer(name='OutQstNet').output])
        else:
            self.net = Model(inputs=[Iin, SoC], outputs=Vout)

            # Define the output functions
            self.VistFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input],
                                      [full_network.get_layer(name='OutIstNet').output])

            self.VdynFnc = K.function([full_network.get_layer(name='Iin').input, full_network.get_layer(name='SoC').input],
                                      [full_network.get_layer(name='OutDynNet').output])

            self.VqstFnc = K.function([full_network.get_layer(name='SoC').input],
                                      [full_network.get_layer(name='OutQstNet').output])

    def fit(self, x_tr, y_tr, nEpoch=2000, batchSize=1, optimizer='Nadam', loss='mse'):
        self.net.compile(optimizer=optimizer, loss=loss)
        history = self.net.fit(x_tr, y_tr, epochs=nEpoch, batch_size=batchSize, verbose=2)

        return history

    def fit_alternated(self, x_tr, y_tr, nGlobalEpoch=67, nLocalEpoch=10, batchSize=1, optimizer='Nadam', loss='mse'):

        time_callback = TimeHistory()
        totalCompileTime = 0
        lossHistory = []

        for globalEpoch in range(nGlobalEpoch):
            print('Instantaneous Network:', globalEpoch + 1, '/', nGlobalEpoch)
            self.net.get_layer(name='HidIstNet').layer.trainable = True
            self.net.get_layer(name='OutIstNet').layer.trainable = True
            self.net.get_layer(name='HidDynNet').trainable = False
            self.net.get_layer(name='OutDynNet').layer.trainable = False
            self.net.get_layer(name='HidQstNet').layer.trainable = False
            self.net.get_layer(name='OutQstNet').layer.trainable = False

            # Train full network
            localWatch = time.time()
            self.net.compile(optimizer=optimizer, loss=loss)
            history = self.net.fit(x_tr, y_tr, epochs=nLocalEpoch, batch_size=batchSize, verbose=2,
                                       callbacks=[time_callback])
            localWatch = time.time() - localWatch
            totalCompileTime += localWatch - time_callback.trainTime

            lossHistory = np.append(lossHistory, history.history['loss'])

            print('Dynamic Network:', globalEpoch + 1, '/', nGlobalEpoch)
            self.net.get_layer(name='HidIstNet').layer.trainable = False
            self.net.get_layer(name='OutIstNet').layer.trainable = False
            self.net.get_layer(name='HidDynNet').trainable = True
            self.net.get_layer(name='OutDynNet').layer.trainable = True
            self.net.get_layer(name='HidQstNet').layer.trainable = False
            self.net.get_layer(name='OutQstNet').layer.trainable = False

            # Train full network
            localWatch = time.time()
            self.net.compile(optimizer=optimizer, loss=loss)
            history = self.net.fit(x_tr, y_tr, epochs=nLocalEpoch, batch_size=batchSize, verbose=2,
                                       callbacks=[time_callback])
            localWatch = time.time() - localWatch
            totalCompileTime += localWatch - time_callback.trainTime

            lossHistory = np.append(lossHistory, history.history['loss'])

            print('Quasi-Stationary Network:', globalEpoch + 1, '/', nGlobalEpoch)
            self.net.get_layer(name='HidIstNet').layer.trainable = False
            self.net.get_layer(name='OutIstNet').layer.trainable = False
            self.net.get_layer(name='HidDynNet').trainable = False
            self.net.get_layer(name='OutDynNet').layer.trainable = False
            self.net.get_layer(name='HidQstNet').layer.trainable = True
            self.net.get_layer(name='OutQstNet').layer.trainable = True

            # Train full network
            localWatch = time.time()
            self.net.compile(optimizer=optimizer, loss=loss)
            history = self.net.fit(x_tr, y_tr, epochs=nLocalEpoch, batch_size=batchSize, verbose=2,
                                       callbacks=[time_callback])
            localWatch = time.time() - localWatch
            totalCompileTime += localWatch - time_callback.trainTime
            lossHistory = np.append(lossHistory, history.history['loss'])

        return history

    def GetWeights(self):
        Vist_hidden = self.net.get_layer(name='HidIstNet').get_weights()
        Vist_out = self.net.get_layer(name='OutIstNet').get_weights()

        Vdyn_hidden = self.net.get_layer(name='HidDynNet').get_weights()
        Vdyn_out = self.net.get_layer(name='OutDynNet').get_weights()

        Vqst_out = self.net.get_layer(name='OutQstNet').get_weights()

        if self.SoC_in is False:
            Vist_hidden[0] = np.concatenate((Vist_hidden[0], np.zeros((1, Vist_hidden[0].shape[1]))), axis=0)
            Vdyn_hidden[0] = np.concatenate((Vdyn_hidden[0], np.zeros((1, Vdyn_hidden[0].shape[1]))), axis=0)

        Vist_w = {'W_i2h': Vist_hidden, 'W_h2o': Vist_out}
        Vdyn_w = {'W_i2h': Vdyn_hidden, 'W_h2o': Vdyn_out}
        Vqst_w = {'num_cheby': float(self.num_cheby), 'num_trig': float(self.num_trig), 'num_bernstein': float(self.num_bernstein),
                  'W_h2o': Vqst_out}

        netWeights = {'Vist_w': Vist_w, 'Vdyn_w': Vdyn_w, 'Vqst_w': Vqst_w}

        self.netWeights = netWeights

        return netWeights

    @staticmethod
    def tau_range_init(shape, name=None, dtype=K.floatx()):
        tau = 10 * np.logspace(1, 3, shape[0], dtype=dtype)
        x = 1 - (K.exp(-1 / tau))
        value = K.log(x / (1 - x))
        return value

class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.trainTime = 0

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.trainTime += (time.time() - self.epoch_time_start)



