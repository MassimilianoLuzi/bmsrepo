from __future__ import unicode_literals
import os
import sys
import json
import time
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from tkinter.filedialog import askopenfilename, asksaveasfilename

from NNE import NNE

# Set seed for repeatability
np.random.seed(7)

# Read options from command line
arguments = sys.argv

### LOAD DATASET ###

# Read Training Set
trainFile = askopenfilename(initialdir=os.path.abspath('../../Dataset/'), title='Load Training Data', defaultextension='mat',
                            filetypes=(("mat file", "*.mat"), ("All Files", "*.*"))) \
    if not '-tr' in arguments else arguments[arguments.index('-tr')+1]

dataTr = scipy.io.loadmat(trainFile)
tr_Time = dataTr['Time']
tr_Iin = dataTr['Iin']
tr_SoC = dataTr['SoC']
tr_Vout = dataTr['Vout']
tr_Temp = dataTr['Temp'] if 'Temp' in dataTr else np.zeros(tr_Iin.shape)
tr_Ts = float(dataTr['Ts']) if 'Ts' in dataTr else 1

# Read Test Set
testFile = askopenfilename(initialdir=os.path.abspath('../DataSet/'), title='Load Test Data', defaultextension='mat',
                           filetypes=(("mat file", "*.mat"), ("All Files", "*.*"))) \
    if not '-ts' in arguments else arguments[arguments.index('-ts')+1]

dataTs = scipy.io.loadmat(testFile)
ts_Time = dataTs['Time']
ts_Iin = dataTs['Iin']
ts_SoC = dataTs['SoC']
ts_Vout = dataTs['Vout']
ts_Temp = dataTs['Temp'] if 'Temp' in dataTs else np.zeros(ts_Iin.shape)
ts_Ts = float(dataTs['Ts']) if 'Ts' in dataTs else 1

assert tr_Ts == ts_Ts

### NORMALIZE ###

# Set parameters for normalization
headroom = 0.1
maxInputVTr = np.max(tr_Vout)
minInputVTr = np.min(tr_Vout)

delta = headroom*(maxInputVTr-minInputVTr)
maxInputVTr += delta/2
minInputVTr -= delta/2

maxInputITr = (1+headroom)*np.max(np.abs(tr_Iin))

# Normalize training set data
tr_Vout = (tr_Vout - minInputVTr) / (maxInputVTr - minInputVTr)
tr_Iin /= maxInputITr
tr_Temp /= 60

# Normalize test set data
ts_Vout = (ts_Vout - minInputVTr) / (maxInputVTr - minInputVTr)
ts_Iin /= maxInputITr
ts_Temp /= 60

### DEFINE INPUTS AND OUTPUTS

# Reshape Training set for final evaluation
inputTr_Iin = np.reshape(tr_Iin.transpose(), (tr_Iin.shape[1], tr_Iin.shape[0], 1))
inputTr_SoC = np.reshape(tr_SoC.transpose(), (tr_SoC.shape[1], tr_SoC.shape[0], 1))
inputTr_Temp = np.reshape(tr_Temp.transpose(), (tr_Temp.shape[1], tr_Temp.shape[0], 1))
outputTr = np.reshape(tr_Vout.transpose(), (tr_Vout.shape[1], tr_Vout.shape[0], 1))

# Reshape Test Set as a tuple fo temporal sequences
inputTs_Iin = np.reshape(ts_Iin.transpose(), (ts_Iin.shape[1], ts_Iin.shape[0], 1))
inputTs_SoC = np.reshape(ts_SoC.transpose(), (ts_SoC.shape[1], ts_SoC.shape[0], 1))
inputTs_Temp = np.reshape(ts_Temp.transpose(), (ts_Temp.shape[1], ts_Temp.shape[0], 1))
outputTs = np.reshape(ts_Vout.transpose(), (ts_Vout.shape[1], ts_Vout.shape[0], 1))

Ts = tr_Ts

### BUILD NETWORK ###
num_neurons_Ist = 10
num_leaky = 5
num_cheby = 0
num_trig = 10
num_bernstein = 20
outputActivation_Qst = 'sigmoid'

SoC_in = True if '-soc' not in arguments else bool(int(arguments[arguments.index('-soc')+1]))
Temp_in = True if '-temp' not in arguments else bool(int(arguments[arguments.index('-temp')+1]))
Temp_in *= not tr_Temp.any()

nne_model = NNE(Ts=Ts, SoC_in=SoC_in, Temp_in=Temp_in,
                  num_neurons_Ist=num_neurons_Ist, num_leaky=num_leaky,
                  num_cheby=num_cheby, num_bernstein=num_bernstein, num_trig=num_trig,
                  outputActivation_Qst=outputActivation_Qst
                  )

### START TRAINING ###
batchSize = 1
nGlobalEpoch = 67
nLocalEpoch = 10

startWatch = time.time()
if Temp_in:
    history = nne_model.fit_alternated([inputTr_Iin, inputTr_SoC, inputTr_Temp], outputTr,
                             nGlobalEpoch=nGlobalEpoch, nLocalEpoch=nLocalEpoch, batchSize=batchSize,
                             optimizer='Nadam', loss='mse')
else:
    history = nne_model.fit_alternated([inputTr_Iin, inputTr_SoC], outputTr,
                             nGlobalEpoch=nGlobalEpoch, nLocalEpoch=nLocalEpoch, batchSize=batchSize,
                             optimizer='Nadam', loss='mse')

### END OF TRAINING ###
trainingTime = time.time()-startWatch
print('Elapsed Time: ', trainingTime)

## Get Network Weights
netWeights = nne_model.GetWeights()

### PERFORMANCE EVALUATION ###
# Training Set
if Temp_in:
    Vout_Train = nne_model.net.predict_on_batch([inputTr_Iin, inputTr_SoC, inputTr_Temp])

    # Evaluate loss on training set
    mse_train = nne_model.net.test_on_batch([inputTr_Iin, inputTr_SoC, inputTr_Temp],
                                             outputTr)

    # Evaluate single components
    Vist_train = nne_model.VistFnc([inputTr_Iin, inputTr_SoC, inputTr_Temp])[0]  # Ist Net
    Vdyn_train = nne_model.VdynFnc([inputTr_Iin, inputTr_SoC, inputTr_Temp])[0]  # Dyn Net
    Vqst_train = nne_model.VqstFnc([inputTr_SoC, inputTr_Temp])[0]  # Qst Net

    # Test Set
    Vout_test = nne_model.net.predict([inputTs_Iin, inputTs_SoC, inputTs_Temp])

    # Evaluate loss on test set
    mse_test = nne_model.net.test_on_batch([inputTs_Iin, inputTs_SoC, inputTs_Temp],
                                            outputTs)

    # Evaluate single components
    Vist_test = nne_model.VistFnc([inputTs_Iin, inputTs_SoC, inputTs_Temp])[0]  # Ist Net
    Vdyn_test = nne_model.VdynFnc([inputTs_Iin, inputTs_SoC, inputTs_Temp])[0]  # Dyn Net
    Vqst_test = nne_model.VqstFnc([inputTs_SoC, inputTs_Temp])[0]  # Qst Net
else:
    Vout_Train = nne_model.net.predict_on_batch([inputTr_Iin, inputTr_SoC])

    # Evaluate loss on training set
    mse_train = nne_model.net.test_on_batch([inputTr_Iin, inputTr_SoC],
                                             outputTr)

    # Evaluate single components
    Vist_train = nne_model.VistFnc([inputTr_Iin, inputTr_SoC])[0]  # Ist Net
    Vdyn_train = nne_model.VdynFnc([inputTr_Iin, inputTr_SoC])[0]  # Dyn Net
    Vqst_train = nne_model.VqstFnc([inputTr_SoC])[0]  # Qst Net

    # Test Set
    Vout_test = nne_model.net.predict([inputTs_Iin, inputTs_SoC])

    # Evaluate loss on test set
    mse_test = nne_model.net.test_on_batch([inputTs_Iin, inputTs_SoC],
                                            outputTs)

    # Evaluate single components
    Vist_test = nne_model.VistFnc([inputTs_Iin, inputTs_SoC])[0]  # Ist Net
    Vdyn_test = nne_model.VdynFnc([inputTs_Iin, inputTs_SoC])[0]  # Dyn Net
    Vqst_test = nne_model.VqstFnc([inputTs_SoC])[0]  # Qst Net


# Save results
if not os.path.exists('Models'):
    os.mkdir('Models')

log = {'Training_Data': os.path.splitext(os.path.basename(trainFile))[0],
       'Test_Data': os.path.splitext(os.path.basename(testFile))[0],
       'Training_Method': 'Alternating Train',
       'Loss_Function': nne_model.net.loss,
       'Optimizer': nne_model.net.optimizer.__str__(),
       'Num_of_Global_Epoch': nGlobalEpoch,
       'Num_of_Local_Epoch': nLocalEpoch,
       'Description': 'Training performed alternating the train of the single networks',
       'Functional_Link': 'Num Cheby neuron:' + str(num_cheby) + "| Num Trig Neuron:" + str(num_trig)}

# Save Model
defaultFileName = os.path.splitext(os.path.basename(trainFile))[0] + '_' + time.ctime().replace(':', '-')
outModelFile = asksaveasfilename(title='Save Model', initialdir=os.path.abspath('Models'), initialfile=defaultFileName,
                                 defaultextension='json', filetypes=(("json file", "*.json"), ("All Files", "*.*")))\
    if not '-os' in arguments else arguments[arguments.index('-os')+1]+defaultFileName
basenameOutPath = os.path.splitext(outModelFile)

model_json_string = nne_model.net.to_json()
jsonModel = json.loads(model_json_string)
jsonModel['input_scaling'] = {'maxInputVTr': maxInputVTr,
               'minInputVTr': minInputVTr,
               'maxInputITr': maxInputITr}
model_json_string = json.dumps(jsonModel)
with open(outModelFile, 'w') as outJsonFile:
    outJsonFile.write(model_json_string)
nne_model.net.save_weights(basenameOutPath[0] + '_Weights.h5')

# Save results in matlab file
scipy.io.savemat(basenameOutPath[0]+'_Matlab.mat', {
    # Log
    'log': log,
    # Normalization Data
    'maxInputVTr': maxInputVTr,
    'minInputVTr': minInputVTr,
    'maxInputITr': maxInputITr,
    # Real Inputs and Outputs Train
    'tr_Time': tr_Time,
    'tr_Iin': tr_Iin,
    'tr_SoC': tr_SoC,
    'tr_Vout': tr_Vout,
    'tr_Temp': tr_Temp,
    # Real Inputs and Outputs Test
    'ts_Time': ts_Time,
    'ts_Iin': ts_Iin,
    'ts_SoC': ts_SoC,
    'ts_Vout': ts_Vout,
    'ts_Temp': ts_Temp,
    # Network Inputs
    'inputTr_Iin': inputTr_Iin,
    'inputTr_SoC': inputTr_SoC,
    'inputTr_Temp': inputTr_Temp,
    'inputTs_Iin': inputTs_Iin,
    'inputTs_SoC': inputTs_SoC,
    'inputTs_Temp': inputTs_Temp,
    # Network Outputs
    'Vout_train': Vout_Train,
    'Vout_test': Vout_test,
    'Vqst_train': Vqst_train,
    'Vist_train': Vist_train,
    'Vdyn_train': Vdyn_train,
    'Vqst_test': Vqst_test,
    'Vist_test': Vist_test,
    'Vdyn_test': Vdyn_test,
    # Network Weights
    'netWeights': netWeights,
    # Network Performance
    'mse_train': mse_train,
    'mse_test': mse_test,
    'trainingTime': trainingTime,
    'lossHistory': history.history['loss']
    })

# Plotting training set results
plotEnable = True if len(arguments) == 1 else False
if plotEnable:
    plt.figure()
    plt.plot(outputTr[0, :])
    plt.plot(Vout_Train[0, :])
    plt.title('Total Estimation - Training Set\n mse: %.4e' % mse_train)
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vist_train[0,])
    plt.title('Instantaneous Contribution Estimation - Training Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vdyn_train[0,])
    plt.title('Dynamic Contribution Estimation - Training Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vqst_train[0,])
    plt.title('Quasi-stationary Contribution Estimation - Training Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=3)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    # Plotting test set results
    plt.figure()
    plt.plot(outputTs[0,])
    plt.plot(Vout_test[0,])
    plt.title('Total Estimation - Test Set\n mse: %.4e' % mse_test)
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vist_test[0,])
    plt.title('Instantaneous Contribution Estimation - Test Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vdyn_test[0,])
    plt.title('Dynamic Contribution Estimation - Test Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()

    plt.figure()
    plt.plot(Vqst_test[0,])
    plt.title('Quasi-stationary Contribution Estimation - Test Set')
    plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
    plt.grid = True
    plt.xlabel('Time [s]')
    plt.ylabel('Normalized Voltage')
    plt.show()
