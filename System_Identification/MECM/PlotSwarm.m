function [fig] = PlotSwarm(obj, iter)
fig=figure(1);
clf;

positions = [obj.swarm.position];

plot(positions, ':o');

color = hsv(length(obj.gBest));
for b = 1:length(obj.gBest)
    plot(obj.gBest(b).position, 'p-', 'MarkerSize', 10, 'color', color(b,:));
end


xlim([1, size(positions,1)]);
hold off;
xlabel('Particle Index');
ylabel('Normalized Value');
title(sprintf('Swarm at iter %d\n\rMSE; %d', iter, obj.gBest(1).fitness));
drawnow;
