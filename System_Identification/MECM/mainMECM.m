% Script for the automatic parameter extraction of a cell from the
% acuisition structured as follows:
%
% Online identification of parameters with PSO algorithm

clear;
close all;
clc;

% Import the HGPSO algorithm. Ensure +heuristic_optimizer is in the MATLAB
% path
import heuristic_optimizer.HGPSO.*

% add path
addpath(genpath('fitness'));


%% Load Training Set
[acquisitionName, acquisitionPath] = uigetfile('Acquisition','Select Training Set', '../../../Dataset');
load(strcat(acquisitionPath,acquisitionName));

tr_Vout = Vout;
tr_Iin = Iin;
tr_Time = Time;

%% Normalize
headroom = 0.1;
maxInputVTr = max(tr_Vout);
minInputVTr = min(tr_Vout);

delta = headroom*(maxInputVTr-minInputVTr);
maxInputVTr = maxInputVTr + delta/2;
minInputVTr = minInputVTr - delta/2;

maxInputITr = (1+headroom)*max(abs(tr_Iin));

tr_Iin = Iin;
tr_Vout = Vout;
tr_SoC = SoC;
tr_Time = Time;

tr_Vout_normalized = (tr_Vout - minInputVTr)/(maxInputVTr-minInputVTr);
tr_Iin_normalized = tr_Iin/maxInputITr;

minSoC = min(tr_SoC);
maxSoC = max(tr_SoC);

%% Load Test Set
[acquisitionName, acquisitionPath] = uigetfile('Acquisition','Select Test Set', '../../../DataSet');
load(strcat(acquisitionPath,acquisitionName));

ts_Iin = Iin;
ts_Vout = Vout;
ts_SoC = SoC;
ts_Time = Time;

ts_Vout_normalized = (ts_Vout - minInputVTr)/(maxInputVTr-minInputVTr);
ts_Iin_normalized = ts_Iin/maxInputITr;

%% Ask for modle configuration
prompt = {...
    'Model Name',           ...
    'Number of RC groups'   ... 
    'Number of Membership Function', ...
    'Max Gaussian Variance',        ...
    };
diagOption.Resize = 'on';
diagOption.WindowsStyle = 'normal';
defaultAnser = {'#ModelName', '3', '15', '0.5'};
promptTitle = 'Enter information about the selected acquisition';
inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption); 

modelName = inputParameters{1};                     % Model name for the output file
numTau = str2double(inputParameters{2});            % Number of RC group in the Dynamic model
numMf = str2double(inputParameters{3});                 % Cell capacity
maxSigma = str2double(inputParameters{4});   % Number of interpolation points for OCV
% Cell capacity Cn should be included in dataset, if not ask to the user
if ~exist('Cn', 'var')
    prompt = {'Cell Capacity'};
    
    diagOption.Resize = 'on';
    diagOption.WindowsStyle = 'normal';
    defaultAnser = {'Cn'};
    promptTitle = 'Enter cell capacity';
    inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption);
    
    % Model name for the output file
    Cn = str2double(inputParameters{1});
end

% Sample time Ts should be included in dataset, if not ask to the user
if ~exist('Ts', 'var')
    prompt = {'Sample Time'};
    
    diagOption.Resize = 'on';
    diagOption.WindowsStyle = 'normal';
    defaultAnser = {'Ts'};
    promptTitle = 'Enter cell capacity';
    inputParameters = inputdlg(prompt,promptTitle, 1,defaultAnser,diagOption);
    
    % Model name for the output file
    Ts = str2double(inputParameters{1});
end
eta = 1;       % Coloumbic Efficiency


%% Configure PSO
% Particle structure
% | Rist, Taudyn(1), Rdyn(1), Taudyn(2), Rdyn(2), ... , Taudyn(numTau), Rdyn(numTau), c(1),s(1), m(1), q(1)...|   

% Popolation bounds
maxTau = 10000;

numParameters =  1 +        ... Rist
                 2*numTau + ... Rdyn tauDyn for each group
                 numMf*4  + ... FIS with numMf gaussian membership functions
                 1;         ... gain

epsilon = 1e-7;
lb = [zeros(1+2*numTau,1);      ... Rist Rdyn tauDyn
      -inf(numMf, 1);           ... centroids
      epsilon*ones(numMf, 1);   ... sigma
      -inf(2*numMf,1);          ... m and q
      -inf;                     ... gain
      ];
Ub = [inf(1+2*numTau,1);        ... Rist Rdyn tauDyn
    inf(numMf, 1);              ... centroids
    maxSigma*ones(numMf, 1);    ... sigma
    inf(2*numMf,1);             ... m and q
    inf;                        ... gain
    ];

initRange_l = [zeros(1+2*numTau,1); ... Rist Rdyn tauDyn
               zeros(numMf, 1);     ... centroids
               zeros(numMf, 1);     ... sigma
               -ones(2*numMf,1);    ... m and q
               0;                   ... gain
               ];
initRange_U = [ones(1+2*numTau,1); ... Rist Rdyn tauDyn
               ones(numMf, 1);     ... centroids
               ones(numMf, 1);     ... sigma
               ones(2*numMf,1);    ... m and q
               1;                  ... gain
               ];
           
%Set algorithm parameters 
generalOptions = struct(...
                'parallelFlag',             true,       ... flag for the parallel evaluation
                'adaptiveFlag',             false,      ... flag for the adaptive velocity update
                'numIndividuals',           50,         ... num individuals of the pso
                'initializationMode',       'uniform',	... type of initialization (zero, uniform, normal)
                'initializationRange',      [initRange_l, initRange_U],     ... range of the initialization
                'lb',                       lb,         ...
                'Ub',                       Ub,         ...
                'numBests',                 2,          ... Number of subswarms
                'killRatio',                0.05,       ... Ratio for kill the subswarm in case of few individuals
                'guaranteedDelta',          0.1,        ... max variation allowed for guaranteed convergence pso
                'maxVelocity',              0.1,        ... max variation allowed for velocity
                'maxIteration',             2000,       ... max num of iteration
                'consecutiveTrueTh',        inf,         ... threshold for the stop condition
                'stopThreshold',            1e-8,       ... threhsold on the difference between old and new gBest
                'fitnessStopThreshold',     1e-8        ... threshold on the difference between old best fitness and new best fitness
                );

% Set PSO parameters
PSOOptions = struct(  ...
    'w',     0.7298,      ...   Inertial coefficient
    'c_p',   1.49618,     ...   Weight of personal best
    'c_g',   1.49618      ...   Weight of global best
    );

GAOptions = struct(  ...
    'kRate',         0.25,            ... kRate for K tournament used in GA hybridization
    'mutationRate',  0.1,             ... Percentage of numIndividuals defining the number of mutation    
    'mutationTh',    0.75,             ... Threshold for the gene to mutate
    'mutationStd',   0.2,             ... Std for mutate the position
    'crossoverRate', 0.1,             ... Percentage of numIndividuals defining the number of crossovers  
    'crossoverTh',   0.5              ... Threshold for the gene to mutate  
    );

% Set Fitness parameters
fitnessOptions = struct(                          ...
    'Iin',                  tr_Iin_normalized,  ... Normalized input current
    'Vout',                 tr_Vout_normalized, ... normalized output voltage
    'numMf',                numMf,              ... num of membership functions
    'numTau',               numTau,             ... num of RC group for modeling the dynamic response
    'maxTau',               maxTau,             ... maximum value for the time constant. To be used only for normalization.
    'maxSigma',             maxSigma,           ... 
    'Cn',                   Cn/maxInputITr,     ... Normalized capacity of the cell
    'eta',                  eta,                ... Coloumbic efficiency of the cell
    'maxInputVTr',          maxInputVTr,        ...
    'minInputVTr',          minInputVTr,        ...
    'Ts',                   Ts                  ... Sampling time
    );

hgpsoOptimizer = HGPSO(@GetFitness_Vqst, numParameters, ...
    'generalOptions',   generalOptions, ...
    'fitnessOptions',   fitnessOptions, ...
    'PSOOptions',       PSOOptions,     ...
    'GAOptions',        GAOptions,      ...
    ...'CallbackPost',         Figure,         ...
    'Verbose',          true);

% Get the dynamic parameters
disp('Running PSO...');
hgpsoOptimizer = hgpsoOptimizer.Initialize();
[hgpsoOptimizer, bestPosition, bestFitness, history, convergenceTime, convergenceIter] = hgpsoOptimizer.Run();

% Get the samples
index = 1;

%% Vist
% Instantaneous resistor
Rist = bestPosition(index);
index = index+1;

%% Vdyn
tauDyn = zeros(fitnessOptions.numTau,1);
Rdyn = zeros(fitnessOptions.numTau,1);
for s=1:fitnessOptions.numTau
    tauGain = 1;...bestPosition(index);
    tauDyn(s)  = tauGain*fitnessOptions.maxTau*bestPosition(index);
    Rdyn(s) =  bestPosition(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
CqstFIS = struct( ...
    'c',    bestPosition(index:index+fitnessOptions.numMf-1), ...
    's',    bestPosition(index+fitnessOptions.numMf:index+2*fitnessOptions.numMf-1), ...
    'm',    bestPosition(index+2*fitnessOptions.numMf:index+3*fitnessOptions.numMf-1),  ...
    'q',    bestPosition(index+3*fitnessOptions.numMf:index+4*fitnessOptions.numMf-1),  ...
    'Cmax', bestPosition(index+4*fitnessOptions.numMf)   ...
    ... 'b',    bestPosition(index+4*fitnessOptions.numMf),    ...
);

%% Store extracted Parameters
disp('Storing extracted parameters...');

minVqst = 3.2;
maxVqst = 4.2;

Vqst_norm = linspace((minVqst-minInputVTr)/(maxInputVTr-minInputVTr),(maxVqst-minInputVTr)/(maxInputVTr-minInputVTr),500);
Vqst = minInputVTr + Vqst_norm*(maxInputVTr-minInputVTr);

% Get Cqst
CqstMf = exp(-(Vqst_norm-CqstFIS.c).^2./(2*CqstFIS.s.^2));
z = Vqst_norm.*CqstFIS.m + CqstFIS.q;
outFIS = sum(CqstMf.*z)./sum(CqstMf);

Cqst_norm = outFIS*CqstFIS.Cmax*1e4;
Cqst = Cqst_norm*maxInputITr/(maxInputVTr-minInputVTr);

% Integrate Vqst for retrieving SoC
trapezArea_norm = (Cqst_norm(2:end)+Cqst_norm(1:end-1)).*(Vqst_norm(2:end)-Vqst_norm(1:end-1))/2;
Qqst_norm(1) = 0;
for n=2:length(trapezArea_norm)
    Qqst_norm(n) = Qqst_norm(n-1) + trapezArea_norm(n-1);
end
Qqst_norm = Qqst_norm/3600;

trapezArea = (Cqst(2:end)+Cqst(1:end-1)).*(Vqst(2:end)-Vqst(1:end-1))/2;
Qqst(1) = 0;
for n=2:length(trapezArea_norm)
    Qqst(n) = Qqst(n-1) + trapezArea(n-1);
end
Qqst = Qqst/3600;

[maxC, maxIndex] = max(Cqst);
Vn = Vqst(maxIndex);
Cn = Qqst(end);

batteryParameters = struct(            ...
    'Vn',           Vn, ...
    'Cn',           Cn, ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'CqstFIS',      CqstFIS,        ...
    'VqstVector',   Vqst_norm(2:end),      ...
    'SoCSamples',   Qqst/Cn);

%% Test extracted parameters
disp('Testing the model...');
[tr_Vout_estimated_normalized, tr_state_normalized] = GetOutputVoltage_Vqst(batteryParameters, numTau, tr_Iin_normalized, tr_Vout_normalized(1), Ts);
tr_mse_normalized = immse(tr_Vout_normalized, tr_Vout_estimated_normalized);

tr_Vout_estimated = minInputVTr + tr_Vout_estimated_normalized*(maxInputVTr - minInputVTr);

figure
plot(tr_Time/3600, tr_Vout);
hold on;
plot(tr_Time/3600, tr_Vout_estimated, 'r');
title(sprintf('Training Set\nMSE: %d', tr_mse_normalized));
xlabel('Time [h]');
ylabel('Voltage [V]');
legend('Measured Voltage', 'Estimated Voltage');

[ts_Vout_estimated_normalized, ts_state_normalized] = GetOutputVoltage_Vqst(batteryParameters, numTau, ts_Iin_normalized, ts_Vout_normalized(1), Ts);
ts_mse = immse(ts_Vout_normalized, ts_Vout_estimated_normalized);

ts_Vout_estimated = minInputVTr + ts_Vout_estimated_normalized*(maxInputVTr - minInputVTr);

figure
plot(ts_Time/3600, ts_Vout);
hold on;
plot(ts_Time/3600, ts_Vout_estimated, 'r');
title(sprintf('Test Set\nMSE: %d', ts_mse));
xlabel('Time [h]');
ylabel('Voltage [V]');
legend('Measured Voltage', 'Estimated Voltage');

mkdir('Output')
save(strcat('Output/batteryParameters_',modelName));





