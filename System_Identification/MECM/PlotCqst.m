function [fig] = PlotCqst(obj, iter)
fig=figure(1);
clf;

numTau = obj.fitnessOptions.numTau;
maxTau = obj.fitnessOptions.maxTau;
numMf = obj.fitnessOptions.numMf;
position = obj.gBest(1).position;

%% Best Vout
% Get the samples
index = 1;

% Vist
% Instantaneous resistor
Rist = position(index);
index = index+1;

%% Vdyn
tauDyn = zeros(numTau,1);
Rdyn = zeros(numTau,1);
for s=1:numTau
%     tauGain = position(index);
    tauDyn(s)  = tauGain*maxTau*position(index);
    Rdyn(s) =  position(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
CqstFIS = struct( ...
    'c',    position(index:index+numMf-1), ...
    's',    position(index+numMf:index+2*numMf-1), ...
    'm',    position(index+2*numMf:index+3*numMf-1),  ...
    'q',    position(index+3*numMf:index+4*numMf-1),  ...
    'Cmax', position(index+4*numMf)   ...
    ...'b',    position(index+4*numMf+1)    ...
);

%% Store extracted Parameters
batteryParameters = struct(              ...
    'Cn',           obj.fitnessOptions.Cn,             ...
    'eta',          obj.fitnessOptions.eta,              ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'CqstFIS',      CqstFIS);

%% Evaluate MSE
% Evaluate Vdyn with the parameters encoded in the position 
% Vout_estimated = GetOutputVoltage_Vqst(batteryParameters, numTau, obj.fitnessOptions.Iin, obj.fitnessOptions.Vout(1), obj.fitnessOptions.Ts);
% 
% subplot(2,1,1);
% plot(obj.fitnessOptions.Vout);
% hold on
% plot(Vout_estimated);

x = linspace(0,1,100);
CqstMf = exp(-(x-batteryParameters.CqstFIS.c).^2./(2*batteryParameters.CqstFIS.s.^2));
z = x.*batteryParameters.CqstFIS.m + batteryParameters.CqstFIS.q; 
outFIS = sum(CqstMf.*z)./sum(CqstMf); ...+ batteryParameters.CqstFIS.b;

Cqst = outFIS*batteryParameters.CqstFIS.Cmax*1e4;
subplot(2,1,1)
plot((CqstMf.*z)');
subplot(2,1,2)
plot(Cqst);
drawnow;