minVqst = 3.2;
maxVqst = 4.2;

Vqst_norm = linspace((minVqst-minInputVTr)/(maxInputVTr-minInputVTr),(maxVqst-minInputVTr)/(maxInputVTr-minInputVTr),500);
Vqst = minInputVTr + Vqst_norm*(maxInputVTr-minInputVTr);

% Get Cqst
CqstMf = exp(-(Vqst_norm-batteryParameters.CqstFIS.c).^2./(2*batteryParameters.CqstFIS.s.^2));
z = Vqst_norm.*batteryParameters.CqstFIS.m + batteryParameters.CqstFIS.q;
outFIS = sum(CqstMf.*z)./sum(CqstMf);

Cqst_norm = outFIS*batteryParameters.CqstFIS.Cmax*1e4;
Cqst = Cqst_norm*maxInputITr/(maxInputVTr-minInputVTr);

% Integrate Vqst for retrieving SoC
trapezArea_norm = (Cqst_norm(2:end)+Cqst_norm(1:end-1)).*(Vqst_norm(2:end)-Vqst_norm(1:end-1))/2;
Qqst_norm(1) = 0;
for n=2:length(trapezArea_norm)
    Qqst_norm(n) = Qqst_norm(n-1) + trapezArea_norm(n-1);
end
Qqst_norm = Qqst_norm/3600;

trapezArea = (Cqst(2:end)+Cqst(1:end-1)).*(Vqst(2:end)-Vqst(1:end-1))/2;
Qqst(1) = 0;
for n=2:length(trapezArea_norm)
    Qqst(n) = Qqst(n-1) + trapezArea(n-1);
end
Qqst = Qqst/3600;

[maxC, maxIndex] = max(Cqst);
Vn = Vqst(maxIndex);
Cn = Qqst(end);

% Plot  Cqst
figure(1)
plot(Vqst,Cqst/1000);
xlabel('Vqst [V]');
ylabel('Cqst [kF]');

% Plot  Qqst
figure(2)
plot(Vqst(1:end-1),Qqst);
xlabel('Vqst [V]');
ylabel('Charge [Ah]');

figure(3)
plot(Qqst_norm/max(Qqst_norm), minInputVTr + Vqst_norm(1:end-1)*(maxInputVTr-minInputVTr));
xlabel('SoC [%]');
ylabel('Vqst [V]');
