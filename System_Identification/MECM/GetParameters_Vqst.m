function [hgpsoOptimizer, batteryParameters, bestFitness, history, convergenceTime, convergenceIter] = GetParameters(Fitness, particleLength, generalOptions, fitnessOptions, PSOOptions, GAOptions, Figure)
% GetParameters: This function run the PSO and retrieve the battery
% parameters encoded in the particle vector.

import optimizerlibrary.PSO.*

%% Run estimation procedure
% Start timer
startWatch = tic;

% Particle structure
% |Rist, Taudyn(1), Rdyn(1), Taudyn(2), Rdyn(2), ... , Taudyn(numTau), Rdyn(numTau) OCV(1),OCV(2), ... , OCV(numOCVSamples), |   

hgpsoOptimizer= HGPSOClass(Fitness, particleLength, ...
    'generalOptions',   generalOptions, ...
    'fitnessOptions',   fitnessOptions, ...
    'PSOOptions',       PSOOptions,     ...
    'GAOptions',        GAOptions,      ...
    'CallbackPost',         Figure,         ...
    'Verbose',          true);

index=1;
% Rist Rdyn tauDyn
initialSwarm(index:index+2*fitnessOptions.numTau,:) = rand(1+2*fitnessOptions.numTau, generalOptions.numIndividuals);
index = index+1+2*fitnessOptions.numTau;

% FIS c
initialSwarm(index:index+fitnessOptions.numMf-1,:) = rand(fitnessOptions.numMf, generalOptions.numIndividuals);
index = index+fitnessOptions.numMf;

% FIS sigma
initialSwarm(index:index+fitnessOptions.numMf-1,:) = fitnessOptions.maxSigma*randn(fitnessOptions.numMf, generalOptions.numIndividuals);
index = index+fitnessOptions.numMf;

% FIS m, q
initialSwarm(index:index+2*fitnessOptions.numMf-1,:) = randn(fitnessOptions.numMf*2, generalOptions.numIndividuals);
index = index+2*fitnessOptions.numMf;

% Gain
initialSwarm(index,:) = rand(1,generalOptions.numIndividuals);
index = index+1;

% % Bias
% initialSwarm(index,:) = rand(1,generalOptions.numIndividuals);

hgpsoOptimizer = hgpsoOptimizer.Initialize(initialSwarm);

[hgpsoOptimizer, bestPosition, bestFitness, history, convergenceTime, convergenceIter] = hgpsoOptimizer.Run();

% Get the samples
index = 1;

%% Vist
% Instantaneous resistor
Rist = bestPosition(index);
index = index+1;

%% Vdyn
tauDyn = zeros(fitnessOptions.numTau,1);
Rdyn = zeros(fitnessOptions.numTau,1);
for s=1:fitnessOptions.numTau
    tauGain = 1;...bestPosition(index);
    tauDyn(s)  = tauGain*fitnessOptions.maxTau*bestPosition(index);
    Rdyn(s) =  bestPosition(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
% CqstFIS = struct( ...
%     'c',    linspace(0,1,fitnessOptions.numMf)', ...,position(index:index+fitnessOptions.numMf-1), ...
%     's',    0.1*ones(fitnessOptions.numMf,1),   ...position(index+fitnessOptions.numMf:index+2*fitnessOptions.numMf-1), ...
%     'y',    position(index:index+fitnessOptions.numMf-1)...position(index+2*fitnessOptions.numMf:index+3*fitnessOptions.numMf-1)  ...
% );

CqstFIS = struct( ...
    'c',    bestPosition(index:index+fitnessOptions.numMf-1), ...
    's',    bestPosition(index+fitnessOptions.numMf:index+2*fitnessOptions.numMf-1), ...
    'm',    bestPosition(index+2*fitnessOptions.numMf:index+3*fitnessOptions.numMf-1),  ...
    'q',    bestPosition(index+3*fitnessOptions.numMf:index+4*fitnessOptions.numMf-1),  ...
    'Cmax', bestPosition(index+4*fitnessOptions.numMf)   ...
    ... 'b',    bestPosition(index+4*fitnessOptions.numMf),    ...
);

%% Store extracted Parameters
disp('Storing extracted parameters...');
batteryParameters = struct(              ...
    'Cn',           fitnessOptions.Cn,             ...
    'eta',          fitnessOptions.eta,              ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'CqstFIS',      CqstFIS);

% Stop time     
convergenceTime = toc(startWatch);
