function  fitness = GetFitness_Vqst( position, fitnessOptions)
% GetFitness: Evaluate the fitness based on Mean Square Error
% input: 
% particle: the current particle of the PSO
% fitnessParam: struct containing the parameters needed for the fitness
% evaluation


%% Get the battery parameters from the current particle
% Particle structure
% |Rist, Taudyn(1), Rdyn(1), Taudyn(2), Rdyn(2), ... , Taudyn(numTau), Rdyn(numTau) OCV(1),OCV(2), ... , OCV(numOCVSamples), |   

% Get the samples
index = 1;

%% Vist
% Instantaneous resistor
Rist = position(index);
index = index+1;

%% Vdyn
tauDyn = zeros(fitnessOptions.numTau,1);
Rdyn = zeros(fitnessOptions.numTau,1);
for s=1:fitnessOptions.numTau
    tauGain = 1; ...position(index);
    tauDyn(s)  = tauGain*fitnessOptions.maxTau*position(index);
    Rdyn(s) =  position(index+1);
    index=index+2;
end
[tauDyn, orderedIndex] = sort(tauDyn);
Rdyn = Rdyn(orderedIndex);

%% Vqst - OCV
CqstFIS = struct( ...
    'c',    position(index:index+fitnessOptions.numMf-1), ...
    's',    position(index+fitnessOptions.numMf:index+2*fitnessOptions.numMf-1), ...
    'm',    position(index+2*fitnessOptions.numMf:index+3*fitnessOptions.numMf-1),  ...
    'q',    position(index+3*fitnessOptions.numMf:index+4*fitnessOptions.numMf-1),  ...
    'Cmax', position(index+4*fitnessOptions.numMf)   ...
    ...'b',    position(index+4*fitnessOptions.numMf+1)    ...
);

%% Store extracted Parameters
batteryParameters = struct(              ...
    'Cn',           fitnessOptions.Cn,             ...
    'eta',          fitnessOptions.eta,              ...
    'Rist',         Rist,           ...
    'Rdyn',         Rdyn,           ...
    'tauDyn',       tauDyn,         ...
    'Cdyn',         tauDyn./Rdyn,   ...
    'CqstFIS',      CqstFIS         ...
    );

%% Evaluate MSE
% Evaluate Vdyn with the parameters encoded in the particle 
Vout_estimated = GetOutputVoltage_Vqst(batteryParameters, fitnessOptions.numTau, fitnessOptions.Iin, fitnessOptions.Vout(1), fitnessOptions.Ts);
mse = immse(fitnessOptions.Vout, Vout_estimated);

% outBounds_s = abs(batteryParameters.CqstFIS.s);
% outBounds_s(outBounds_s < fitnessOptions.maxSigma) = [];

% Get Cqst
minVqst = 3;
maxVqst = 4.2;
maxInputVTr = fitnessOptions.maxInputVTr;
minInputVTr = fitnessOptions.minInputVTr;
Vqst_norm = linspace((minVqst-minInputVTr)/(maxInputVTr-minInputVTr),(maxVqst-minInputVTr)/(maxInputVTr-minInputVTr),500);
CqstMf = exp(-(Vqst_norm-batteryParameters.CqstFIS.c).^2./(2*batteryParameters.CqstFIS.s.^2));
z = Vqst_norm.*batteryParameters.CqstFIS.m + batteryParameters.CqstFIS.q;
outFIS = sum(CqstMf.*z)./sum(CqstMf);
Cqst_norm = outFIS*batteryParameters.CqstFIS.Cmax;
Cqst_norm(Cqst_norm >= 0) = [];

penalty = sum(-Cqst_norm);

fitness = mse + penalty;


end