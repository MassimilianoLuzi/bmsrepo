function Vout = EvaluateOutput_Vqst(state, input, batteryParam)
% EvaluateOutput: This is the measurement function g() of the battery model
% Inputs:
% state: the current state (x[k])
% input: the current input (u[k])
% batteryParam: struct storing all the parameters of the model
%
% Output: the current output voltage (y[k])

Vout = state(1)                     ... Vqst. The OCV-SoC function is implemented through a lookup table 
       + sum(state(2:end))          ... Vdyn. It is the sum of the contribution of each RC group.
       + batteryParam.Rist*input;   ... Vist. It is the ohmic behavior of the cell.
end

