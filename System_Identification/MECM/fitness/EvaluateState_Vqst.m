function outState = EvaluateState_Vqst(state, input, batteryParam, Ts )
% EvaluateState: This is the process function f() of the battery model
% Inputs:
% state: the current state (x[k])
% input: the current input (u[k])
% batteryParam: struct storing all the parameters of the model
%
% output: the next state (x[k+1])

% Get the leghth of states
N = length(state);

% Initialize the variable outState
outState = zeros(N,1);

% The first state is the SoC eavaluated with the Coloumb Counting approach
% (discrete approximation of the integral)
CqstMf = exp(-(state(1)-batteryParam.CqstFIS.c).^2./(2*batteryParam.CqstFIS.s.^2));
z = state(1).*batteryParam.CqstFIS.m + batteryParam.CqstFIS.q; 
outFIS = sum(CqstMf.*z)/sum(CqstMf); % + batteryParam.CqstFIS.b;

Cqst = outFIS*batteryParam.CqstFIS.Cmax*1e4;

outState(1) = state(1) + Ts*input/Cqst;

% The other states refer to each RC group
for n=1:N-1
    % x[k+1] = exp(-1/tau)*x[k] + u[k]*R*(1-exp(-1/tau))
    outState(n+1) = exp(-Ts/batteryParam.tauDyn(n))*state(n+1) + input*batteryParam.Rdyn(n)*(1-exp(-Ts/batteryParam.tauDyn(n)));
end
end

