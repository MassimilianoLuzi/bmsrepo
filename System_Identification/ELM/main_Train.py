from __future__ import unicode_literals
import os
import sys
import json
import time
import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from tkinter.filedialog import askopenfilename, asksaveasfilename

from ELM import ELM

# Read options from command line
arguments = sys.argv

# Set seed for repeatability
np.random.seed(7)

### LOAD DATASET ###

# Read Training Set
trainFile = askopenfilename(initialdir=os.path.abspath('../../Dataset/'), title='Load Training Data', defaultextension='mat',
                            filetypes=(("mat file", "*.mat"), ("All Files", "*.*"))) \
    if not '-tr' in arguments else arguments[arguments.index('-tr')+1]

dataTr = scipy.io.loadmat(trainFile)
tr_Time = dataTr['Time']
tr_Iin = dataTr['Iin']
tr_SoC = dataTr['SoC']
tr_Vout = dataTr['Vout']
tr_Temp = dataTr['Temp'] if 'Temp' in dataTr else np.zeros(tr_Iin.shape)
tr_Ts = float(dataTr['Ts']) if 'Ts' in dataTr else 1

# Read Test Set
testFile = askopenfilename(initialdir=os.path.abspath('../DataSet/'), title='Load Test Data', defaultextension='mat',
                           filetypes=(("mat file", "*.mat"), ("All Files", "*.*"))) \
    if not '-ts' in arguments else arguments[arguments.index('-ts')+1]

dataTs = scipy.io.loadmat(testFile)
ts_Time = dataTs['Time']
ts_Iin = dataTs['Iin']
ts_SoC = dataTs['SoC']
ts_Vout = dataTs['Vout']
ts_Temp = dataTs['Temp'] if 'Temp' in dataTs else np.zeros(ts_Iin.shape)
ts_Ts = float(dataTs['Ts']) if 'Ts' in dataTs else 1

assert tr_Ts == ts_Ts

### NORMALIZE ###

# Set parameters for normalization
headroom = 0.1
maxInputVTr = np.max(tr_Vout)
minInputVTr = np.min(tr_Vout)

delta = headroom*(maxInputVTr-minInputVTr)
maxInputVTr += delta/2
minInputVTr -= delta/2

maxInputITr = (1+headroom)*np.max(np.abs(tr_Iin))

# Normalize training set data
tr_Vout = (tr_Vout - minInputVTr) / (maxInputVTr - minInputVTr)
tr_Iin /= maxInputITr
tr_Temp /= 60

# Normalize test set data
ts_Vout = (ts_Vout - minInputVTr) / (maxInputVTr - minInputVTr)
ts_Iin /= maxInputITr
ts_Temp /= 60

### DEFINE INPUTS AND OUTPUTS

# Shift inputs for voltage feedback - Training
inputTr_Iin = tr_Iin[1:]
inputTr_Vout = tr_Vout[0:-1]
inputTr_SoC = tr_SoC[1:]
inputTr_Temp = tr_Temp[1:]
outputTr = tr_Vout[1:]

# Shift inputs for voltage feedback - Test
inputTs_Iin = ts_Iin[1:]
inputTs_Vout = ts_Vout[0:-1]
inputTs_SoC = ts_SoC[1:]
inputTs_Temp = ts_Temp[1:]
outputTs = ts_Vout[1:]


### BUILD NETWORK ###
Temp_in = True if '-temp' not in arguments else bool(int(arguments[arguments.index('-temp')+1]))
Temp_in *= not tr_Temp.any()
numNeurons = 10
hiddenActivation = 'tanh'
outputActivation = 'linear'
hiddenInitializer = 'uniform'
biasInitializer = 'uniform'
elm_model = ELM(Temp_in=Temp_in,
                 numNeurons=numNeurons,
                 hiddenActivation=hiddenActivation, outputActivation=outputActivation,
                 hiddenInitializer=hiddenInitializer, biasInitializer=biasInitializer
                 )

### START TRAINING ###
# Build the input and output tuples
if Temp_in:
    inputTr = np.concatenate((inputTr_Iin, inputTr_Vout, inputTr_SoC, inputTr_Temp), axis=1)
    inputTs = np.concatenate((inputTs_Iin, inputTs_Vout, inputTs_SoC, inputTs_Temp), axis=1)
else:
    inputTr = np.concatenate((inputTr_Iin, inputTr_Vout, inputTr_SoC), axis=1)
    inputTs = np.concatenate((inputTs_Iin, inputTs_Vout, inputTs_SoC), axis=1)

startWatch = time.time()

elm_model.fit(inputTr, outputTr)

### END OF TRAINING ###
trainingTime = time.time()-startWatch
print('Elapsed Time: ', trainingTime)

## Get Network Weights
netWeights = elm_model.GetWeights()

### PERFORMANCE EVALUATION ###
# Training Set

# Evaluate performance on the test set
outputTraining = elm_model.net.predict_on_batch(inputTr)
# Evaluate loss on training set
totalLossTraining = elm_model.net.test_on_batch(inputTr, outputTr)

# Test Set
# Evaluate performance on the test set
outputTest = elm_model.net.predict_on_batch(inputTs)
# Evaluate loss on training set
totalLossTest=elm_model.net.test_on_batch(inputTs, outputTs)

# Save results
if not os.path.exists('Models'):
    os.mkdir('Models')

log = {'Training Data': os.path.splitext(os.path.basename(trainFile))[0],
       'Test Data': os.path.splitext(os.path.basename(testFile))[0],
       'Training Method': 'Least Square',
       'Loss Function': elm_model.net.loss,
       'Optimizer': elm_model.net.optimizer.__str__(),
       'Description': 'Model based on Extreme Learning Machine'}

# Save Model
defaultFileName = os.path.splitext(os.path.basename(trainFile))[0] + '_' + time.ctime().replace(':', '-')
outModelFile = asksaveasfilename(title='Save Model', initialdir=os.path.abspath('Models'), initialfile=defaultFileName,
                                 defaultextension='json', filetypes=(("json file", "*.json"), ("All Files", "*.*")))
basenameOutPath = os.path.splitext(outModelFile)

model_json_string = elm_model.net.to_json()
jsonModel = json.loads(model_json_string)
jsonModel['input_scaling'] = {'maxInputVTr': maxInputVTr,
               'minInputVTr': minInputVTr,
               'maxInputITr': maxInputITr}
model_json_string = json.dumps(jsonModel)
with open(outModelFile, 'w') as outJsonFile:
    outJsonFile.write(model_json_string)
elm_model.net.save_weights(basenameOutPath[0]+'_Weights.h5')

# Save results in matlab file
scipy.io.savemat(basenameOutPath[0]+'_Matlab.mat', {
    # Log
    'log': log,
    # Normalization Data
    'maxInputVTr': maxInputVTr,
    'minInputVTr': minInputVTr,
    'maxInputITr': maxInputITr,
    # Real Inputs and Outputs Train
    'tr_Time': tr_Time,
    'tr_Iin': tr_Iin,
    'tr_SoC': tr_SoC,
    'tr_Vout': tr_Vout,
    # Real Inputs and Outputs Test
    'ts_Time': ts_Time,
    'ts_Iin': ts_Iin,
    'ts_SoC': ts_SoC,
    'ts_Vout': ts_Vout,
    # Network Inputs
    'inputTr': inputTr,
    # Network Outputs
    'outTrain': outputTraining,
    'outTest': outputTest,
    # Network Weights
    'netWeights': netWeights,
    # Network Performance
    'trainMse': totalLossTraining,
    'testMse': totalLossTest,
    'trainingTime': trainingTime
    })

# Plotting training set results
plt.figure()
plt.plot(outputTr)
plt.plot(outputTraining)
plt.title('Total Estimation - Training Set\n mse: %.4e' % totalLossTraining)
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()

# Plotting test set results
plt.figure()
plt.plot(outputTs)
plt.plot(outputTest)
plt.title('Total Estimation - Test Set\n mse: %.4e' % totalLossTest)
plt.legend(['Real Voltage', 'Estimated Voltage'], loc=4)
plt.grid = True
plt.xlabel('Time [s]')
plt.ylabel('Normalized Voltage')
plt.show()
