import time

import numpy as np

from keras.models import Sequential
from keras.layers import Dense, TimeDistributed
from keras import backend as K

import sys



class ELM:
    def __init__(self, Temp_in,
                 numNeurons=10,
                 hiddenActivation='tanh', outputActivation='linear',
                 hiddenInitializer='uniform', biasInitializer='uniform'
                 ):

        # Save parameters
        # General parameters
        self.Temp_in = Temp_in

        # ELM net
        self.numNeurons = numNeurons
        self.hiddenActivation = hiddenActivation
        self.outputActivation = outputActivation
        self.hiddenInitializer = hiddenInitializer
        self.biasInitializer = biasInitializer

        ### BUILD NETWORK ###
        numInput = 3+self.Temp_in

        self.net = Sequential()
        self.net.add(Dense(self.numNeurons, activation=self.hiddenActivation,
                           kernel_initializer=self.hiddenInitializer, bias_initializer=self.biasInitializer,
                           trainable=False, input_dim=numInput, name='hiddenLayer'))
        self.net.add(Dense(1, activation='linear', kernel_initializer='glorot_normal', name='outputLayer'))

        self.HidFnc = K.function([self.net.get_layer(name='hiddenLayer').input],
                                 [self.net.get_layer(name='hiddenLayer').output])

    def fit(self, x_tr, y_tr, optimizer='Nadam', loss='mse'):
        self.net.compile(optimizer=optimizer, loss=loss)

        hidden = self.HidFnc([x_tr])[0]
        A = np.concatenate((hidden, np.ones((len(x_tr), 1))), axis=-1)
        y = np.linalg.lstsq(A, y_tr)

        netWeights = [y[0][0:-1], y[0][-1]]
        self.net.get_layer(name='outputLayer').set_weights(netWeights)

        history = y[1]
        return history

    def fit_bp(self, x_tr, y_tr, nEpoch=2000, batchSize=1, optimizer='Nadam', loss='mse'):
        self.net.compile(optimizer=optimizer, loss=loss)

        history = self.net.fit(x_tr, y_tr, epochs=nEpoch, batch_size=batchSize,
                      verbose=2)

        return history

    def GetWeights(self):
        hidden_w = self.net.get_layer(name='hiddenLayer').get_weights()
        out_w = self.net.get_layer(name='outputLayer').get_weights()
        netWeights = {'W_i2h': hidden_w, 'W_h2o': out_w}
        self.netWeights = netWeights
        return netWeights