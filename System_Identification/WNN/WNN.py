import time

import numpy as np

from keras.models import Sequential
from keras.layers import Dense, TimeDistributed
from keras import backend as K

from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import euclidean_distances

import sys
sys.path.append('../../Libraries/Python')

from customLayers import Wavelet

class WNN:
    def __init__(self, Temp_in,
                 numNeurons=10,
                 ):

        # Save parameters
        # General parameters
        self.Temp_in = Temp_in

        # ELM net
        self.numNeurons = numNeurons

        ### BUILD NETWORK ###
        self.numInput = 3+self.Temp_in

        self.net = Sequential()
        self.net.add(Wavelet(self.numNeurons, input_dim=self.numInput, name='hiddenLayer'))
        self.net.add(Dense(1, activation='linear', kernel_initializer='glorot_normal', name='outputLayer'))

    def fit(self, x_tr, y_tr, nEpoch=2000, batchSize=32, optimizer='Nadam', loss='mse'):
        self.net.compile(optimizer=optimizer, loss=loss)

        kmeans = KMeans(n_clusters=self.numNeurons)
        kmeans.fit(np.concatenate((x_tr, y_tr), axis=1))
        translation = kmeans.cluster_centers_[:, 0:self.numInput]
        dilatation = translation.copy()
        for n in range(10):
            distance = euclidean_distances(x_tr[kmeans.labels_ == n], translation[None, n, :])
            dilatation[n] = np.abs(x_tr[kmeans.labels_ == n][np.argmax(distance)] - translation[n,])

        self.net.layers[0].initialize_wavelons(translation, dilatation)

        history = self.net.fit(x_tr, y_tr, epochs=nEpoch, batch_size=batchSize, verbose=2)

        return history

    def GetWeights(self):
        hidden_w = self.net.get_layer(name='hiddenLayer').get_weights()
        out_w = self.net.get_layer(name='outputLayer').get_weights()
        netWeights = {'W_i2h': hidden_w, 'W_h2o': out_w}
        self.netWeights = netWeights
        return netWeights