Ts=21e-3;
startIndex = 1+round(0/Ts);
stopIndex = 1+round(Time(end)/Ts);

Cn = 40;

Iin = Iin(startIndex:stopIndex);
Vout = Vout(startIndex:stopIndex);
if exist('Q_Ah', 'var')
    Q_Ah = Q_Ah(startIndex:stopIndex);
else
    Q_Ah = zeros(length(Iin),1);
end
if exist('SoC_CC', 'var')
    SoC_CC = SoC_CC(startIndex:stopIndex);
else
    SoC_CC = zeros(length(Iin),1);
end
if exist('chargingFlag', 'var')
    chargingFlag = chargingFlag(startIndex:stopIndex);
else
    chargingFlag = zeros(length(Iin),1);
end
if exist('relayFlag', 'var')    
    relayFlag = relayFlag(startIndex:stopIndex);
    Iin(relayFlag==0) = 0;
else
    relayFlag = zeros(length(Iin),1);
end
Time = Time(startIndex:stopIndex)-Time(startIndex);

SoC(1) = 1;
for n=2:length(Time)
SoC(n) = SoC(n-1)+Iin(n)*(Time(n)-Time(n-1))/(Cn*3600);
end

Ts=1;

sim('untitled');