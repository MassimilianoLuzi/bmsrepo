%%% UKF script

close all;
clear;
clc;
 
% Add path
addpath('../../Libraries');
addpath('../../Models');

% Import Libriries.
import('kalmanfilter.SRUKF');
import('CellModels.MECM');

%% Load model parameters
[modelName, modelPath] = uigetfile('Model','Select Model', '../Models');
load(strcat(modelPath,modelName));

cell = MECM(batteryParameters, Ts);

% Load Test Set
[testName, testPath] = uigetfile('Test set','Select Test Set', '../../../Dataset');
load(strcat(testPath,testName));

startIndex = 1+0.5*3600;

Iin_n = Iin/maxInputITr;
Vout_n = (Vout - minInputVTr)/(maxInputVTr - minInputVTr);

signalLength = length(Iin);

% Sampling time
Ts = 1;

% Initial Vqst
Vqst0 = Vout_n(startIndex);

% Set initial mean state
X0 = [(3.7-minInputVTr)/(maxInputVTr-minInputVTr);
      0 ;
      0 ;
      0];

% State dimension
N = length(X0);
numInputs = 1;
numOutputs = 1;

% Set initial mean covariance   
Pqst = 0.1;
Pdyn = diag([1e-4, 1e-4, 1e-4]);
P0 = blkdiag(Pqst, Pdyn);

% Set error covariance matricies
Qqst = 1e-10;
Qdyn = diag([1e-12, 1e-12, 1e-12]);
Q = chol(blkdiag(Qqst,Qdyn));
R = sqrt(10^-5);

% Set UKF parameters
alpha = 0.5;
beta = 2;
kappa = 0;

X = zeros(signalLength, N);
Y = zeros(signalLength, 1);
SoC_estimated = zeros(signalLength, 1);

modelOptions = [];

lb = [-inf; -inf(numTau,1)];
Ub = [inf;  inf(numTau,1)];
VqstEstimator = SRUKF(N, numInputs, numOutputs,  ...
        @cell.ProcessFunction, @cell.MeasurementFunction,   ...
        'lb',       lb,                                     ...
        'Ub',       Ub,                                     ...
        'X0',       X0,                                     ...
        'P0',       P0,                                     ...
        'Q',        Q,                                      ...
        'R',        R,                                      ...
        'alpha',    alpha,                                  ...
        'beta',     beta,                                   ...
        'kappa',    kappa);
X(1,:) = VqstEstimator.X;
Y(1) = VqstEstimator.Y;

% Start UKF
iterTime = zeros(signalLength,1);
for k = 2:signalLength
    if k>startIndex
        timer = tic;
        VqstEstimator = VqstEstimator.FilterStep(Iin_n(k), Vout_n(k)); 
        iterTime(k) = toc(timer);
    end
    X(k,:) = VqstEstimator.X;
    Y(k) = VqstEstimator.Y;
    SoC_estimated(k) = LookupTable(X(k,1), cell.cellParameters.VqstVector, cell.cellParameters.SoCSamples, 'linear');
    
%     if mod(k,3600) == 0
%         plot((1:k)/3600, SoC_estimated(1:k,1)*100, (1:k)/3600, SoC(1:k,1)*100); 
%         drawnow;
%     end
end

plot(Time/3600, SoC, Time(startIndex:end)/3600, SoC_estimated(startIndex:end)); 
xlabel('Time [h]');
ylabel('SoC [%]');
SoCmse = immse(SoC_estimated(startIndex:end),SoC(startIndex:end));

figure
Vout_estimated = Y;
plot(Time/3600, Vout, ...
    Time(startIndex:end)/3600, minInputVTr + Vout_estimated(startIndex:end)*(maxInputVTr-minInputVTr)); 
xlabel('Time [h]');
ylabel('Voltage [V]');

avgTime = mean(iterTime(startIndex:end));

fprintf('SoC MSE: %.2e - Avg Iter Time: %.2e s\n', SoCmse, avgTime);


