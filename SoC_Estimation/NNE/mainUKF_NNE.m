%%% UKF script

close all;
clear;
clc;
 
% Add path
addpath('../../Libraries');
addpath('../../Models');

% Import Libriries.
import('kalmanfilter.SRUKF');
import('CellModels.NNE');

% Load model parameters
[testName, testPath] = uigetfile('Model','Select Model', '../Results/Official/');
load(strcat(testPath,testName));

prompt = {'Cell Capacity'};

diagOption.Resize = 'on';
diagOption.WindowsStyle = 'normal';
defaultAnser = {'Cn'};
promptTitle = 'Enter the capacity of the cell';
inputParameters = inputdlg(prompt, promptTitle, 1,defaultAnser,diagOption); 

% Model name for the output file
Cn = str2double(inputParameters{1});

Cn = Cn/maxInputITr;
eta = 1;
Ts = 1;
numLeaky = length(netWeights.Vdyn_w.W_h2o);
tempFlag = size(netWeights.Vist_w.W_i2h, 2) == 3;

cell = NNE(netWeights, Cn, eta, Ts);

% Load Test Set
[testName, testPath] = uigetfile('Test set','Select Test Set', '../../Dataset');
load(strcat(testPath,testName));

startIndex = 1+1*3600; 

Iin_n = Iin/maxInputITr;
Vout_n = (Vout - minInputVTr)/(maxInputVTr - minInputVTr);
if  exist('Temp', 'var')
    Temp_n = Temp/100;
end

signalLength = length(Iin);

% Initial SoC
s0 = 0.5;

% Set initial mean state
X0 = [s0; zeros(numLeaky, 1)];

% State dimension
N = length(X0);
if tempFlag
    numInputs = 2;
else
    numInputs = 1;
end
numOutputs = 1;

% Set initial mean covariance   
Psoc = 0.1;
Pdyn = 1e-4;
Pdyn = diag(Pdyn*ones(numLeaky,1));
P0 = blkdiag(Psoc, Pdyn);

% Set error covariance matricies
Qsoc = 1e-10;
Qdyn = 1e-12;
Qdyn = diag(Qdyn*ones(numLeaky,1));
Q = chol(blkdiag(Qsoc,Qdyn));
R = sqrt(10^-5);

% Set UKF parameters
alpha = 0.5;
beta = 2;
kappa = 0;

X = zeros(signalLength, N);
Y = zeros(signalLength, 1);

X_n = zeros(signalLength, N);
Y_n = zeros(signalLength, 1);

modelOptions = struct( ...
    'Ts',  1 ...
);

lb = [0; -inf(numLeaky,1)];
Ub = [1.02;  inf(numLeaky,1)];

if tempFlag
    ProccessFnc = @cell.ProcessFunction_Temp;
    MeasurementFnc = @cell.MeasurementFunction_Temp;
else
    ProccessFnc = @cell.ProcessFunction;
    MeasurementFnc = @cell.MeasurementFunction;
end
SoCEstimator = SRUKF(N, numInputs, numOutputs,  ...
        ProccessFnc, MeasurementFnc,   ...
        'lb',       lb,                                     ...
        'Ub',       Ub,                                     ...
        'X0',       X0,                                     ...
        'P0',       P0,                                     ...
        'Q',        Q,                                      ...
        'R',        R,                                      ...
        'alpha',    alpha,                                  ...
        'beta',     beta,                                   ...
        'kappa',    kappa);

X(1,:) = SoCEstimator.X;
Y(1) = SoCEstimator.Y;

% Start UKF
if tempFlag
    input = [Iin_n, Temp_n];
else
    input = Iin_n;
end
figure(1);
plt = plot((1:1)/3600, X(1:1,1)*100, (1:1)/3600, SoC(1:1,1)*100); 
iterTime = zeros(signalLength,1);
for k = 2:signalLength
    if k>startIndex
        timer = tic;
        SoCEstimator = SoCEstimator.FilterStep(input(k,:), Vout_n(k)); 
        iterTime(k) = toc(timer);
    end
    X(k,:) = SoCEstimator.X;
    Y(k) = SoCEstimator.Y;
    
%     if mod(k,3600) == 0
%         set(plt(1), 'YData', X(1:k,1)*100);
%         set(plt(1), 'XData',  (1:k)/3600);
%         set(plt(2), 'YData', SoC(1:k,1)*100);
%         set(plt(2), 'XData',  (1:k)/3600);
%         drawnow;
%     end
end

SoC_estimated = X(:,1);
plot(Time/3600, SoC, Time(startIndex:end)/3600, SoC_estimated(startIndex:end)); 
xlabel('Time [h]');
ylabel('SoC [%]');
SoCmse = immse(SoC_estimated(startIndex:end),SoC(startIndex:end));

figure
Vout_estimated = Y;
plot(Time/3600, Vout, ...
    Time(startIndex:end)/3600, minInputVTr + Vout_estimated(startIndex:end)*(maxInputVTr-minInputVTr)); 
xlabel('Time [h]');
ylabel('Voltage [V]');

avgTime = mean(iterTime(startIndex:end));

fprintf('SoC MSE: %.2e - Avg Iter Time: %.2e s\n', SoCmse, avgTime);
